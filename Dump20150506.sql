CREATE DATABASE  IF NOT EXISTS `security-model` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `security-model`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: security-model
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id_idx` (`country_id`),
  CONSTRAINT `country_id` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Amman',1),(2,'Irbid5',1),(3,'Riyadh',4),(4,'Makka',4);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Jordan'),(4,'Saudi Arabia');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_proj`
--

DROP TABLE IF EXISTS `emp_proj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp_proj` (
  `emp_id` int(11) NOT NULL,
  `proj_id` int(11) NOT NULL,
  `is_project_lead` int(1) DEFAULT '0',
  PRIMARY KEY (`emp_id`,`proj_id`),
  KEY `proj_id_idx` (`proj_id`),
  CONSTRAINT `emp_id` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `proj_id` FOREIGN KEY (`proj_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_proj`
--

LOCK TABLES `emp_proj` WRITE;
/*!40000 ALTER TABLE `emp_proj` DISABLE KEYS */;
INSERT INTO `emp_proj` VALUES (1,1,0),(1,2,0),(2,1,0),(3,1,0);
/*!40000 ALTER TABLE `emp_proj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'ibrahim','sawalha'),(2,'adnan','sawalha'),(3,'rayan','sawalha');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'BANKS'),(2,'GL');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_application`
--

DROP TABLE IF EXISTS `sys_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_application` (
  `APP_NO` tinyint(4) NOT NULL DEFAULT '0',
  `APP_NAME` varchar(60) NOT NULL,
  `APP_DESC` varchar(200) DEFAULT NULL,
  `APP_STATUS` varchar(1) DEFAULT NULL,
  `APP_ORDER` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`APP_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_application`
--

LOCK TABLES `sys_application` WRITE;
/*!40000 ALTER TABLE `sys_application` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_form`
--

DROP TABLE IF EXISTS `sys_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_form` (
  `MOD_NO` tinyint(4) NOT NULL DEFAULT '0',
  `APP_NO` tinyint(4) NOT NULL DEFAULT '0',
  `FRM_NO` int(11) NOT NULL DEFAULT '0',
  `FRM_NAME` varchar(60) NOT NULL,
  `FRM_ENAME` varchar(60) DEFAULT NULL,
  `FRM_OBJECT` varchar(100) NOT NULL DEFAULT '',
  `FRM_INSERT` varchar(1) DEFAULT NULL,
  `FRM_UPDATE` varchar(1) DEFAULT NULL,
  `FRM_DELETE` varchar(1) DEFAULT NULL,
  `FRM_QUERY` varchar(1) DEFAULT NULL,
  `FRM_PRINT` varchar(1) DEFAULT NULL,
  `FRM_APPROV` varchar(1) DEFAULT NULL,
  `FRM_UNAPPROV` varchar(1) DEFAULT NULL,
  `FRM_STATUS` varchar(1) DEFAULT NULL,
  `FORM_ICON` longblob,
  PRIMARY KEY (`FRM_NO`,`MOD_NO`,`APP_NO`),
  UNIQUE KEY `FRM_PK` (`FRM_NO`,`MOD_NO`,`APP_NO`),
  KEY `FRM_MOD_1_FK` (`MOD_NO`,`APP_NO`),
  CONSTRAINT `FRM_MOD_1_FK` FOREIGN KEY (`MOD_NO`, `APP_NO`) REFERENCES `sys_module` (`MOD_NO`, `APP_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_form`
--

LOCK TABLES `sys_form` WRITE;
/*!40000 ALTER TABLE `sys_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_group`
--

DROP TABLE IF EXISTS `sys_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_group` (
  `GRP_NO` int(11) NOT NULL DEFAULT '0',
  `GRP_NAME` varchar(60) NOT NULL DEFAULT '',
  `GRP_STATUS` varchar(1) DEFAULT NULL,
  `GRP_ENAME` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`GRP_NO`),
  UNIQUE KEY `GRP_PK` (`GRP_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_group`
--

LOCK TABLES `sys_group` WRITE;
/*!40000 ALTER TABLE `sys_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_module`
--

DROP TABLE IF EXISTS `sys_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_module` (
  `APP_NO` tinyint(4) NOT NULL DEFAULT '0',
  `MOD_NO` tinyint(4) NOT NULL,
  `MOD_NAME` varchar(60) NOT NULL,
  `MOD_DESC` varchar(200) DEFAULT NULL,
  `MOD_STATUS` varchar(1) DEFAULT NULL,
  `ORDER_BY` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`MOD_NO`,`APP_NO`),
  UNIQUE KEY `MOD_1_PK` (`MOD_NO`,`APP_NO`),
  KEY `MOD_1_APP_FK` (`APP_NO`),
  CONSTRAINT `MOD_1_APP_FK` FOREIGN KEY (`APP_NO`) REFERENCES `sys_application` (`APP_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_module`
--

LOCK TABLES `sys_module` WRITE;
/*!40000 ALTER TABLE `sys_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_privileg`
--

DROP TABLE IF EXISTS `sys_privileg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_privileg` (
  `APP_NO` tinyint(4) NOT NULL DEFAULT '0',
  `FRM_NO` int(11) NOT NULL DEFAULT '0',
  `GRP_NO` int(11) NOT NULL DEFAULT '0',
  `MOD_NO` tinyint(4) NOT NULL DEFAULT '0',
  `PRV_INSERT` varchar(1) DEFAULT NULL,
  `PRV_DELETE` varchar(1) DEFAULT NULL,
  `PRV_QUERY` varchar(1) DEFAULT NULL,
  `PRV_UPDATE` varchar(1) DEFAULT NULL,
  `PRV_PRINT` varchar(1) DEFAULT NULL,
  `PRV_APPROV` varchar(1) DEFAULT NULL,
  `PRV_UNAPPROV` varchar(1) DEFAULT NULL,
  `PRV_STATUS` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`MOD_NO`,`GRP_NO`,`FRM_NO`,`APP_NO`),
  KEY `FK_sys_privileg_mod` (`APP_NO`,`MOD_NO`),
  KEY `FK_sys_privileg_grp` (`GRP_NO`),
  KEY `FK_sys_privileg_frm` (`MOD_NO`,`APP_NO`,`FRM_NO`),
  CONSTRAINT `FK_sys_privileg_app` FOREIGN KEY (`APP_NO`) REFERENCES `sys_application` (`APP_NO`),
  CONSTRAINT `FK_sys_privileg_frm` FOREIGN KEY (`MOD_NO`, `APP_NO`, `FRM_NO`) REFERENCES `sys_form` (`MOD_NO`, `APP_NO`, `FRM_NO`),
  CONSTRAINT `FK_sys_privileg_grp` FOREIGN KEY (`GRP_NO`) REFERENCES `sys_group` (`GRP_NO`),
  CONSTRAINT `FK_sys_privileg_mod` FOREIGN KEY (`APP_NO`, `MOD_NO`) REFERENCES `sys_module` (`APP_NO`, `MOD_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_privileg`
--

LOCK TABLES `sys_privileg` WRITE;
/*!40000 ALTER TABLE `sys_privileg` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_privileg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `user_id` varchar(30) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `aname` varchar(200) NOT NULL DEFAULT '',
  `ename` varchar(200) DEFAULT NULL,
  `Status` int(11) NOT NULL DEFAULT '0',
  `password` varchar(45) NOT NULL DEFAULT '',
  `GRP_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `FK_sys_user_grp` (`GRP_NO`),
  CONSTRAINT `FK_sys_user_grp` FOREIGN KEY (`GRP_NO`) REFERENCES `sys_group` (`GRP_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'security-model'
--

--
-- Dumping routines for database 'security-model'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-06 11:55:08
