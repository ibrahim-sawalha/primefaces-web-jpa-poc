package cdi.beans;

public interface Service {
    int doWork(int a, int b);
}
