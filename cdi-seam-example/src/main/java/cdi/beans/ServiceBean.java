package cdi.beans;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class ServiceBean implements Serializable {

    private String actionName = "init";
    private int i = 0;

    @Inject
    private TestBean testBean;

    public int doWork(int a, int b) {
        i += a + b;
        return i;
    }

    public String getName() {
        return "Last Value:" + i;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }
}
