package cdi.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class TestBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<String> items;
    private String item;
    private String serviceValue;

    @Inject
    private ServiceBean service;

    @PostConstruct
    private void init() {
        items = new ArrayList<>();
        items.add("Item 1");
        items.add("Item 2");
        items.add("Item 3");
    }

    public void addItem() {
        if (item != null && !item.isEmpty()) {
            items.add(item);
            item = null;
        }
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public List<String> getItems() {
        return items;
    }

    public String getServiceValue() {
        serviceValue = service.getName();
        return serviceValue;
    }

}
