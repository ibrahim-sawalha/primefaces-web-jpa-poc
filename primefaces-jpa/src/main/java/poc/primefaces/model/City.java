package poc.primefaces.model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the city database table.
 *
 */
@Entity
@NamedQuery(name = "City.findAll", query = "SELECT c FROM City c")
@Cacheable(false)
public class City implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    private String name;

    // bi-directional many-to-one association to Country
    @ManyToOne
    private Country country;

    public City() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("City [id=");
        builder.append(id);
        builder.append(", name=");
        builder.append(name);
        builder.append("]");
        return builder.toString();
    }

}
