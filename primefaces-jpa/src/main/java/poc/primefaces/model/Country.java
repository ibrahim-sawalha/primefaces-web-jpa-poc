package poc.primefaces.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The persistent class for the country database table.
 *
 */
@Entity
@NamedQuery(name = "Country.findAll", query = "SELECT c FROM Country c")
@XmlRootElement
@Cacheable(false)
public class Country implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    private String name;

    @XmlElement(nillable = true)
    // bi-directional many-to-one association to City
    @OneToMany(mappedBy = "country", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<City> cities;

    public Country() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCities() {
        return this.cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public City addCity(City city) {
        getCities().add(city);
        city.setCountry(this);

        return city;
    }

    public City removeCity(City city) {
        getCities().remove(city);
        city.setCountry(null);

        return city;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Country [id=");
        builder.append(id);
        builder.append(", name=");
        builder.append(name);
        builder.append(", cities=");
        builder.append(cities);
        builder.append("]");
        return builder.toString();
    }

}
