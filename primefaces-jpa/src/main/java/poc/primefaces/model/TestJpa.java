package poc.primefaces.model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TestJpa {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnit-hibernate");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // Country found = entityManager.find(Country.class, 1);
        // System.out.println("found=" + found);

        Employee emp = entityManager.find(Employee.class, 1);
        System.out.println("emp=" + emp);
        System.out.println("emp.getProjects=" + emp.getProjects());

        Project project = entityManager.find(Project.class, 1);
        System.out.println("project=" + project);
        System.out.println("getEmployee=" + project.getEmployee());
        System.out.println("getEmployees=" + project.getEmployees());

        Employee newEmp = entityManager.find(Employee.class, 3);
        project.addEmployee(newEmp);
        entityManager.getTransaction().begin();
        entityManager.merge(project);
        entityManager.getTransaction().commit();

    }

}
