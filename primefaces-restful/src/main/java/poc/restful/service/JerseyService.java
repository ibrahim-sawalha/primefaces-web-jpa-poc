package poc.restful.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.WebServiceException;

import poc.primefaces.model.Country;

@Path("/service")
public class JerseyService {

    @POST
    @Path("/post")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCountry(Country c) {
        try {
            Country country = new Country();
            country.setId(1000);
            country.setName("Jordan");
            return Response.status(200).entity(country).build();
        } catch (WebServiceException e) {
            e.printStackTrace();
            return Response.status(500).entity("Error:" + e.getMessage()).build();
        }
    }

    @GET
    @Path("/get")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCountryGET() {
        try {
            Country country = new Country();
            country.setId(1000);
            country.setName("Jordan");
            return Response.status(200).entity(country.toString()).build();
        } catch (WebServiceException e) {
            e.printStackTrace();
            return Response.status(500).entity("Error:" + e.getMessage()).build();
        }
    }
}
