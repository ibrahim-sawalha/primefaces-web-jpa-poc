package poc.restful.service.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import poc.primefaces.model.Country;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class TestService {

    public static String urlLocal = "http://localhost:8787/primefaces-restful/rest/service/post";
    public static String getUrl = "http://localhost:8787/primefaces-restful/rest/service/get";

    public void doGet() {
        try {

            Client client = Client.create();
            WebResource webResource = client.resource(getUrl);
            ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            String output = response.getEntity(String.class);

            System.out.println(output);

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    public void doPost() {
        try {
            Country c = new Country();
            c.setId(2000);
            c.setName("Saudi Arabia");

            DefaultClientConfig defaultClientConfig = new DefaultClientConfig();
            defaultClientConfig.getClasses().add(JacksonJsonProvider.class);
            Client client = Client.create(defaultClientConfig);

            WebResource webResource = client.resource(urlLocal);

            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, c);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            // System.out.println(getStringFromInputStream(response.getEntityInputStream()));
            Country output = response.getEntity(Country.class);
            System.out.println("From Server:...");
            System.out.println(output);
        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    // convert InputStream to String
    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    public static void main(String[] args) {
        new TestService().doPost();
        // new TestService().doGet();
    }

}
