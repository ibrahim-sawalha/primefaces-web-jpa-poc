package poc.jsf.utils;

import java.util.HashMap;
import java.util.Map;

public class ArabicAbgadConverter {

	private static Map<Integer, String> map;

	public static String convertNumberToChar(Integer val) throws Exception {
		if (val == null) {
			throw new Exception("Integer Number is null");
		}
		if (val < 1 || val > 28) {
			throw new Exception(
					"Integer Number is not in range 1..28, your input is:"
							+ val);
		}
		if (map == null) {
			map = new HashMap<>();
			map.put(1, "أ");
			map.put(2, "ب");
			map.put(3, "ج");
			map.put(4, "د");
			map.put(5, "هـ");
			map.put(6, "و");

			map.put(7, "ز");
			map.put(8, "ح");
			map.put(9, "ط");
			map.put(10, "ي");
			map.put(11, "ك");
			map.put(12, "ل");
			map.put(13, "م");
			map.put(14, "ن");
			map.put(15, "س");
			map.put(16, "ع");
			map.put(17, "ف");
			map.put(18, "ص");
			map.put(19, "ق");
			map.put(20, "ر");
			map.put(21, "ش");
			map.put(22, "ت");
			map.put(23, "ث");
			map.put(24, "خ");
			map.put(25, "ذ");
			map.put(26, "ض");
			map.put(27, "ظ");
			map.put(28, "غ");
		}
		String str = map.get(val);
		return str;

	}

}
