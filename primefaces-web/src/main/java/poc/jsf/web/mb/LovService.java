package poc.jsf.web.mb;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import poc.primefaces.model.City;
import poc.primefaces.model.Country;

@Stateless
public class LovService implements Serializable {

    // @PersistenceContext(type = PersistenceContextType.EXTENDED)
    private static EntityManager manager;

    public List<Country> getAllCountries() {
        String sql = "SELECT l FROM Country l";
        Query query = getEntityManager().createQuery(sql);
        return query.getResultList();
    }

    public static EntityManager getEntityManager() {
        if (manager == null) {
            EntityManagerFactory entityManagerFactory = Persistence
                    .createEntityManagerFactory("persistenceUnit-hibernate");
            manager = entityManagerFactory.createEntityManager();
        }
        return manager;
    }

    public void saveCountry(Country country) {
        EntityTransaction tx = getEntityManager().getTransaction();
        tx.begin();
        // getEntityManager().persist(country);
        getEntityManager().merge(country);
        tx.commit();
    }

    public void deleteCountry(Country country) {
        EntityTransaction tx = getEntityManager().getTransaction();
        tx.begin();
        getEntityManager().remove(country);
        tx.commit();
    }

    public void saveCity(City city) {
        EntityTransaction tx = getEntityManager().getTransaction();
        tx.begin();
        // getEntityManager().persist(country);
        getEntityManager().merge(city);
        tx.commit();
    }

    public void deleteCity(City city) {
        EntityTransaction tx = getEntityManager().getTransaction();
        tx.begin();
        getEntityManager().remove(city);
        tx.commit();
    }

}
