package poc.jsf.web.mb;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import poc.primefaces.model.City;
import poc.primefaces.model.Country;

@ManagedBean(name = "mbCountry")
@ViewScoped
public class MbCountry implements Serializable {
    private static final long serialVersionUID = -4705413463984696703L;

    private Country selectedCountry;
    private City selectedCity;

    public List<Country> getCountries() {
        LovService service = new LovService();
        return service.getAllCountries();
    }

    public List<City> getCitites(Country country) {
        System.out.println("get String : " + country);
        if (country != null) {
            return country.getCities();
        }
        return null;
    }

    public Country getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(Country selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public City getSelectedCity() {
        return selectedCity;
    }

    public void setSelectedCity(City selectedCity) {
        this.selectedCity = selectedCity;
    }

    public static EntityManager getEM() {
        return Persistence.createEntityManagerFactory("persistenceUnit-hibernate").createEntityManager();
    }

}
