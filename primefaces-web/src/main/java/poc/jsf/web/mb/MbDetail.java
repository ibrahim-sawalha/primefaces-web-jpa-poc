package poc.jsf.web.mb;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import poc.primefaces.model.City;
import poc.primefaces.model.Country;

@ManagedBean(name = "mbDetailCRUD")
@ViewScoped
public class MbDetail implements Serializable {
    private static final long serialVersionUID = -190668462909860319L;
    private Country selectedCountry;
    private City selectedCity;
    private List<Country> countries;
    private boolean addCity = false;

    private LovService service = new LovService();

    @PostConstruct
    public void init() {
        selectedCountry = (Country) JsfUtils.getRequestAttribute("selectedCountry");
        selectedCity = (City) JsfUtils.getRequestAttribute("currentCity");
        addCity = (Boolean) JsfUtils.getRequestAttribute("isNewCity");
        System.out.println(getClass().getName() + ":init --> " + selectedCountry);
        System.out.println(getClass().getName() + ":init --> " + addCity);
    }

    public Country getSelectedCountry() {
        System.out.println("getSelectedCountry---> " + selectedCountry);
        if (selectedCountry == null) {
            selectedCountry = new Country();
        }
        return selectedCountry;
    }

    public void setSelectedCountry(Country selectedCountry) {
        if (selectedCountry != null) {
            System.out.println("set selectedCountry" + selectedCountry.getName());
        }
        this.selectedCountry = selectedCountry;
    }

    public City getSelectedCity() {
        if (selectedCity == null) {
            selectedCity = new City();
        }
        return selectedCity;
    }

    public void setSelectedCity(City selectedCity) {
        System.out.println("setSelectedCity " + selectedCity);
        this.selectedCity = selectedCity;
    }

    public List<Country> getCountries() {
        System.out.println("getCountries...");
        countries = service.getAllCountries();
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public void onCountryRowSelect(SelectEvent event) {
        setSelectedCity(null);
        FacesMessage msg = new FacesMessage("Country Selected", ((Country) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCountryRowUnselect(UnselectEvent event) {
        System.out.println(event.getObject());
        FacesMessage msg = new FacesMessage("Country Unselected", ((Country) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCityRowSelect(SelectEvent event) {
        System.out.println(event.getObject());
        FacesMessage msg = new FacesMessage("City Selected", ((City) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCityRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("City Unselected", ((City) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void createCountry(ActionEvent e) {
        addCity = true;
        selectedCountry = null;
        selectedCity = null;
    }

    public String updateCityAction() {
        addCity = false;
        System.out.println("selectedCity =" + selectedCity);
        return "detail_crud.xhtml";
    }

    public void createCity(ActionEvent e) {
        addCity = true;
        selectedCity = null;
    }

    public String createCityAction() {
        addCity = true;
        selectedCity = null;
        return "detail_crud.xhtml";
    }

    public void saveCity(ActionEvent e) {
        System.out.println("Selected City --> " + selectedCity);
        if (selectedCountry != null) {
            service.saveCountry(selectedCountry);
        }
        addCity = false;
    }

    public String saveCityAction() {
        System.out.println("Selected City --> " + selectedCity);
        System.out.println("Selected City --> " + selectedCity.getCountry());
        if (selectedCountry != null) {
            if (addCity) {
                selectedCountry.addCity(selectedCity);
            }
            service.saveCountry(selectedCountry);
        }
        addCity = false;
        return "index3.xhtml";
    }

    public String saveCityActionAndAdd() {
        if (selectedCountry != null) {
            if (addCity) {
                selectedCountry.addCity(selectedCity);
            }
            service.saveCountry(selectedCountry);
        }
        addCity = true;
        selectedCity = new City();
        selectedCity.setCountry(selectedCountry);
        return null;
    }

    public void deleteCity(ActionEvent e) {
        // selectedCountry.getCities().remove(selectedCity);
        selectedCountry.removeCity(selectedCity);
        selectedCity = null;
    }

    public String deleteCityAction() {
        System.out.println("Delete City -->" + selectedCity);
        selectedCountry.removeCity(selectedCity);
        service.deleteCity(selectedCity);
        selectedCity = null;
        return "index3.xhtml";
    }

    public boolean isAddCity() {
        return addCity;
    }

    public void setAddCity(boolean addCity) {
        this.addCity = addCity;
    }

    public String goHome() {
        return "index3.xhtml?faces-redirect=true";
    }

}
