package poc.jsf.web.mb;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.omnifaces.cdi.Param;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import poc.primefaces.model.City;
import poc.primefaces.model.Country;

@ManagedBean(name = "mbMasterCRUD")
@ViewScoped
public class MbMaster implements Serializable {
    private static final long serialVersionUID = -190668462909860319L;
    @Param(name = "c")
    private Country selectedCountry;
    private boolean addCountry = false;

    private LovService service = new LovService();

    @PostConstruct
    public void init() {
        try {
            selectedCountry = (Country) JsfUtils.getRequestAttribute("currentCountry");
            addCountry = (Boolean) JsfUtils.getRequestAttribute("isNewCountry");
        } catch (NullPointerException e) {
            addCountry = true;
        }
        System.out.println("MbMaster.init --> " + selectedCountry);
        System.out.println("MbMaster.init --> " + addCountry);
    }

    public Country getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(Country selectedCountry) {
        System.out.println("MbMaster.setSelectedCountry---> " + selectedCountry);
        if (selectedCountry != null) {
            System.out.println("set selectedCountry" + selectedCountry.getName());
            this.selectedCountry = selectedCountry;
        }
        this.selectedCountry = selectedCountry;
    }

    public void onCountryRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Country Selected", ((Country) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCountryRowUnselect(UnselectEvent event) {
        System.out.println(event.getObject());
        FacesMessage msg = new FacesMessage("Country Unselected", ((Country) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCityRowSelect(SelectEvent event) {
        System.out.println(event.getObject());
        FacesMessage msg = new FacesMessage("City Selected", ((City) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCityRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("City Unselected", ((City) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void createCountry(ActionEvent e) {
        addCountry = true;
        selectedCountry = null;
    }

    public String newCountry() {
        addCountry = true;
        selectedCountry = null;
        return "master_crud.xhtml";
    }

    public String updateCountry() {
        System.out.println("MbMaster.updateCountry>>>> " + selectedCountry);
        return "master_crud.xhtml";
    }

    public String saveCountry() {
        service.saveCountry(selectedCountry);
        JsfUtils.setRequestAttribute("currentCountry", selectedCountry);
        addCountry = false;
        return "index3.xhtml";
    }

    public void deleteCountry(ActionEvent e) {
        System.out.println("MbMaster.deleteCountry-->" + selectedCountry);
        service.deleteCountry(selectedCountry);
        selectedCountry = null;
    }

    public boolean getAddCountry() {
        return addCountry;
    }

    public void setAddCountry(boolean addCountry) {
        this.addCountry = addCountry;
    }

    public String goHome() {
        JsfUtils.setRequestAttribute("currentCountry", selectedCountry);
        return "index3.xhtml";
    }

    public String action() {
        return "master_crud.xhtml";
    }

}
