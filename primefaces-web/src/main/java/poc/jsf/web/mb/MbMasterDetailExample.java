package poc.jsf.web.mb;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.omnifaces.cdi.Param;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import poc.primefaces.model.City;
import poc.primefaces.model.Country;

@ManagedBean(name = "mbMasterDetail")
@ViewScoped
public class MbMasterDetailExample implements Serializable {
    private static final long serialVersionUID = -190668462909860319L;

    @Param(name = "c")
    private Country selectedCountry;

    private City selectedCity;
    private List<Country> countries;
    private boolean addCountry = true;
    private boolean addCity = false;

    private LovService service = new LovService();

    @PostConstruct
    public void init() {
        selectedCountry = (Country) JsfUtils.getRequestAttribute("currentCountry");
        selectedCity = (City) JsfUtils.getRequestAttribute("currentCity");
    }

    public Country getSelectedCountry() {
        if (selectedCountry == null) {
            selectedCountry = new Country();
        }
        return selectedCountry;
    }

    public void setSelectedCountry(Country selectedCountry) {
        if (selectedCountry != null) {
            System.out.println("mbMasterDetail.set selectedCountry" + selectedCountry.getName());
        }
        this.selectedCountry = selectedCountry;
    }

    public City getSelectedCity() {
        if (selectedCity == null) {
            selectedCity = new City();
        }
        return selectedCity;
    }

    public void setSelectedCity(City selectedCity) {
        System.out.println("mbMasterDetail.setSelectedCity " + selectedCity);
        this.selectedCity = selectedCity;
    }

    public List<Country> getCountries() {
        if (countries == null) {
            System.out.println("mbMasterDetail.getCountries...");
            countries = service.getAllCountries();
        }
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public void onCountryRowSelect(SelectEvent event) {
        JsfUtils.setExpressionValue("#{mbMasterCRUD.selectedCountry}", event.getObject());
        setSelectedCity(null);
        FacesMessage msg = new FacesMessage("Country Selected", ((Country) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCountryRowUnselect(UnselectEvent event) {
        System.out.println(event.getObject());
        FacesMessage msg = new FacesMessage("Country Unselected", ((Country) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCityRowSelect(SelectEvent event) {
        System.out.println(event.getObject());
        FacesMessage msg = new FacesMessage("City Selected", ((City) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCityRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("City Unselected", ((City) event.getObject()).getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void createCountry(ActionEvent e) {
        addCountry = true;
        selectedCountry = null;
        selectedCity = null;
    }

    public String newCountry() {
        JsfUtils.setRequestAttribute("currentCountry", new Country());
        JsfUtils.setRequestAttribute("isNewCountry", true);
        addCountry = true;
        selectedCountry = null;
        selectedCity = null;
        return "master_crud.xhtml";
    }

    public String updateCountry() {
        JsfUtils.setRequestAttribute("currentCountry", selectedCountry);
        JsfUtils.setRequestAttribute("isNewCountry", false);
        addCountry = false;
        return "master_crud.xhtml";
    }

    public String saveCountry() {
        System.out.println("mbMasterDetail.addCountry?" + addCountry);
        System.out.println("mbMasterDetail.selectedCountry:" + selectedCountry);
        service.saveCountry(selectedCountry);
        if (addCountry) { // create new country
            System.out.println(selectedCountry);
            countries.add(selectedCountry);
        } else { // update existing country
            for (Country c : countries) {
                if (c.getId() == selectedCountry.getId()) {
                    System.out.println(c.getName());
                    c.setName(selectedCountry.getName());
                }
            }
        }
        addCountry = false;
        return "index3.xhtml";
    }

    public void deleteCountry(ActionEvent e) {
        System.out.println("mbMasterDetail.deleteCountry-->" + selectedCountry);
        service.deleteCountry(selectedCountry);
        countries.remove(selectedCountry);
        selectedCountry = null;
    }

    public void createCity(ActionEvent e) {
        addCity = true;
        selectedCity = null;
    }

    public String updateCityAction() {
        JsfUtils.setRequestAttribute("selectedCountry", selectedCountry);
        JsfUtils.setRequestAttribute("selectedCity", selectedCity);
        JsfUtils.setRequestAttribute("isNewCity", false);
        return "detail_crud.xhtml";
    }

    public String createCityAction() {
        addCity = true;
        selectedCity = null;
        System.out.println(getClass().getName() + ":createCityAction:" + selectedCountry);
        JsfUtils.setRequestAttribute("selectedCountry", selectedCountry);
        JsfUtils.setRequestAttribute("selectedCity", null);
        JsfUtils.setRequestAttribute("isNewCity", true);
        return "detail_crud.xhtml";
    }

    public void saveCity(ActionEvent e) {
        if (selectedCountry != null) {
            System.out.println("mbMasterDetail.Selected City --> " + selectedCity);
            // if (addCity) {
            // System.out.println("mbMasterDetail.NEW");
            // selectedCountry.addCity(selectedCity);
            // } else {
            // System.out.println("mbMasterDetail.UPDATE");
            // for (City city : getSelectedCountry().getCities()) {
            // if (city.getId() == selectedCity.getId()) {
            // city = selectedCity;
            // }
            // }
            // }
            service.saveCountry(selectedCountry);
        }
        addCity = false;
    }

    public String saveCityAction() {
        System.out.println("mbMasterDetail.Selected City --> " + selectedCity);
        System.out.println("mbMasterDetail.Selected City --> " + selectedCity.getCountry());
        if (selectedCountry != null) {
            if (addCity) {
                selectedCountry.addCity(selectedCity);
            }
            service.saveCountry(selectedCountry);
        }
        addCity = false;
        return "index3.xhtml";
    }

    public String saveCityActionAndAdd() {
        if (selectedCountry != null) {
            if (addCity) {
                selectedCountry.addCity(selectedCity);
            }
            service.saveCountry(selectedCountry);
        }
        addCity = true;
        selectedCity = new City();
        selectedCity.setCountry(selectedCountry);
        return null;
    }

    public void deleteCity(ActionEvent e) {
        // selectedCountry.getCities().remove(selectedCity);
        selectedCountry.removeCity(selectedCity);
        selectedCity = null;
    }

    public String deleteCityAction() {
        System.out.println("mbMasterDetail.Delete City -->" + selectedCity);
        selectedCountry.removeCity(selectedCity);
        service.deleteCity(selectedCity);
        selectedCity = null;
        return "index3.xhtml";
    }

    public boolean getAddCountry() {
        return addCountry;
    }

    public void setAddCountry(boolean addCountry) {
        this.addCountry = addCountry;
    }

    public boolean isAddCity() {
        return addCity;
    }

    public void setAddCity(boolean addCity) {
        this.addCity = addCity;
    }

    public String goHome() {
        return "index3.xhtml";
    }

}
