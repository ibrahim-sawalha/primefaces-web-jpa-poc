ALTER TABLE `security-model`.`city` 
DROP FOREIGN KEY `country_id`;
ALTER TABLE `security-model`.`city` 
ADD CONSTRAINT `country_id`
  FOREIGN KEY (`country_id`)
  REFERENCES `security-model`.`country` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
